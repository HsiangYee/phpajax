<?php
    include('conn.php');

    $sql = "SELECT * FROM message";
    $result = $db->query($sql);
    $data = array();
    while ($message = mysqli_fetch_assoc($result)) {
        $tmp = array(
            "id" => $message['id'],
            'message' => $message['message'],
        );

        array_push($data, $tmp);
    }

    $response = array(
        'status' => 'success',
        'message' => $data,
    );
    echo json_encode($response);