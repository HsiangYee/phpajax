function input_isNull(input) {
    let isNull = false;
    if (input.val().trim().length == 0) {
        isNull = true;
    }

    return isNull;
}

function input_is_invalid(input) {
    let id = input.attr('id');
    $(`#${id}-invalid-message`).html('不得空白');
    if (input.val().trim().length == 0 && !input.hasClass('is-invalid')) {
        input.toggleClass('is-invalid');

    } else if (input.val().trim().length != 0 && input.hasClass('is-invalid')) {
        input.removeClass('is-invalid');
    }
}

function invalid_message(input, message) {
    let id = input.attr('id');
    $(`#${id}-invalid-message`).html(message);
    if (!input.hasClass('is-invalid')) {
        input.toggleClass('is-invalid');
    }
}

function cancel_invalid(input) {
    let id = input.attr('id');
    $(`#${id}-invalid-message`).html("");
    if (input.hasClass('is-invalid')) {
        input.removeClass('is-invalid');
    }
}